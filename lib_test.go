package yfinlib

import (
	"testing"
)

func TestGetCurrentStockPrice(t *testing.T) {

	stockPrice := GetCurrentStockPrice("SPY")
	t.Log("GetCurrentStockPrice(SPY)=", stockPrice)
	t.Errorf("Error: Testing GetCurrentStockPrice")
}

func TestCreateYahooFinanceOptionTickerSymbol(t *testing.T) {

	optionTicker := CreateYahooFinanceOptionTickerSymbol("SPY", "160408", "P", 205.50)
	t.Log("CreateYahooFinanceOptionTickerSymbol(SPY, 160408, P, 205.50)=", optionTicker)
	t.Errorf("Error: Testing CreateYahooFinanceOptionTickerSymbol")
}
