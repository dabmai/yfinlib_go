package yfinlib

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
)

// Use my mac on firefox as the user agent when scraping yahoo finance
const USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:43.0) Gecko/20100101 Firefox/43.0"

func check(specialMsg string, e error) {
	if e != nil {
		//panic(e)
		log.Println(specialMsg)
		log.Println(e)
	}
}

func GetCurrentStockPrice(tickerSymbol string) float64 {

	client := &http.Client{}

	url := "http://finance.yahoo.com/q/op?s=" + tickerSymbol

	// Create request with the url
	req, err := http.NewRequest("GET", url, nil)
	check("ERROR: GetCurrentStockPrice() - http.NewRequest()", err)

	// Set the User Agent to Firefox MacOs
	req.Header.Set("User-Agent", USER_AGENT)

	// Do the request
	resp, err := client.Do(req)
	check("ERROR: GetCurrentStockPrice() - client.Do()", err)

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	check("ERROR: GetCurrentStockPrice() - ioutil.ReadAll", err)

	bodyString := string(body)

	// Look for this pattern: SPY:value">201.32</span>
	startStringToFind := tickerSymbol + ":value\">"
	endStringToFind := "</span>"

	intPos1 := strings.Index(bodyString, startStringToFind)

	if intPos1 != -1 {

		intPos1 = intPos1 + len(startStringToFind)
		intPos2 := strings.Index(bodyString[intPos1:], endStringToFind) + intPos1

		if intPos2 != -1 {

			return StringToFloat(bodyString[intPos1:intPos2])
		}
	}

	// not found
	return 99999.99
}

func StringToFloat(myString string) float64 {

	retFloat, err := strconv.ParseFloat(strings.Trim(myString, "\n"), 64)

	if err != nil {
		fmt.Println("ERROR: StringToFloat( ", myString, " ): ", err)
	}

	return retFloat
}

func CreateYahooFinanceOptionTickerSymbol(stockSymbol string, expireDateStringYYMMDD string, CorP string, strikePriceFloat float64) string {

	//AAPL160219C00105000

	strikePriceString := ""

	strikePrice := strconv.FormatFloat(strikePriceFloat, 'f', 1, 64)

	if strings.Contains(strikePrice, ".5") {
		strikePrice = strings.Replace(strikePrice, ".5", "50", 1)
	} else {
		strikePrice = strings.Replace(strikePrice, ".", "0", 1)
	}

	if len(strikePrice) == 3 {
		strikePriceString += "0000" + strikePrice + "0"
	} else if len(strikePrice) == 4 {
		strikePriceString += "000" + strikePrice + "0"
	} else if len(strikePrice) == 5 {
		strikePriceString += "00" + strikePrice + "0"
	} else if len(strikePrice) == 6 {
		strikePriceString += "0" + strikePrice + "0"
	}

	return stockSymbol + expireDateStringYYMMDD + CorP + strikePriceString
}
